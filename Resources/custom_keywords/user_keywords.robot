*** Settings ***
Documentation    Suite description
Resource    /Users/shiprashekhar/synaptic/Resources/page_objects/common_settings.robot


*** Keywords ***
Multiple user array
    ${json_string1}=    catenate
     ...      [{
     ...             "id": 456 ,
     ...             "username": "ss131ss",
     ...             "firstName": "Shipra",
     ...             "lastName": "Shekhar",
     ...             "email": "test@xyz.com",
     ...             "password": "123456",
     ...             "phone": "9292222282",
     ...             "userStatus": 0
     ...      }]
   Set Global Variable        ${json_string_new1}		     ${json_string1}


Create multiple users with array
| |  Multiple user array           |                                   |                                 |                                   |                           |                      |
| |  Create Session	               |  tmd                              |  ${url} 	                     |                                   |                           |                      |
| |  ${headers}=                   |  Create Dictionary                |  Content-Type=application/json  |                                   |                           |                      |
| |  ${resp}=                      |  Post Request	                   |  tmd	                         |  /user/createWithArray            | data=${json_string_new1}  |  headers=${headers}  |
| |  Should Be Equal As Strings	   |  ${resp.status_code}              |  200                            |                                   |                           |                      |
| |  Log to console 	           |  ${resp.content}                  |                                 |                                   |                           |                      |



Update user with new details
   Create Session	             tmd                      ${url}
   &{body}=                      Create Dictionary        id=456  username=${username}  firstName=Shipra1  lastName=Shekhar1  email=newship@xyz.com  password=XXXXX  phone=9292222289  userStatus= 1
   ${headers}=                   Create Dictionary        Content-Type=application/json
   ${resp}=                      Put Request	          tmd	                           /user/ss131ss   data=&{body}   headers=${headers}
   Should Be Equal As Strings	 ${resp.status_code}      200
   Log to console 	             ${resp.content}


Get updated user details
| |  Create Session	               | 	tmd                                   |  ${url} 	                     |                     |                   |
| |  ${header}=                    |    Create Dictionary                     |  Content-Type=application/json   |                     |                   |
| |  ${resp}= 	                   |    Get Request                           |  tmd                             |  /user/${username}  | headers=${header} |
| |  &{data} =                     |    Evaluate                              |  json.loads($resp.text)          |  json               |                   |
| |  Set Global Variable           |    ${new_firstName}                      |  &{data}[firstName]              |                     |                   |
| |  Set Global Variable           |    ${new_email}                          |  &{data}[email]                  |                     |                   |
| |  Set Global Variable           |    ${new_password}                       |  &{data}[password]               |                     |                   |
| |  Should Be Equal As Strings	   |    ${new_firstName}                      |  Shipra1                         |                     |                   |
| |  Should Be Equal As Strings    |    ${new_email}                          |  newship@xyz.com                 |                     |                   |
| |  Should Be Equal As Strings    |    ${new_password}                       |  XXXXX                           |                     |                   |
| |  Log to console 	           |    ${resp.content}                       |                                  |                     |                   |
