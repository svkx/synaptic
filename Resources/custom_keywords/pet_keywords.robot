*** Settings ***
Documentation    Suite description
Resource    /Users/shiprashekhar/synaptic/Resources/page_objects/common_settings.robot


*** Keywords ***
Pet details
    ${json_string2}=    catenate
     ...             {
     ...               "id": 10,
     ...               "category": {
     ...                 "id": 60,
     ...                 "name": "dog"
     ...               },
     ...               "name": "doggie",
     ...               "photoUrls": [
     ...                 "string"
     ...               ],
     ...               "tags": [
     ...                 {
     ...                   "id": 0,
     ...                   "name": "string"
     ...                 }
     ...               ],
     ...               "status": "available"
     ...             }
   Set Global Variable        ${json_string_new2}		     ${json_string2}



Create pet details
| |  Pet details                    |                                   |                                 |         |                            |                      |
| |  Create Session	                |  tmd                              |  ${url} 	                      |         |                            |                      |
| |  ${headers}=                    |  Create Dictionary                |  Content-Type=application/json  |         |                            |                      |
| |  ${resp}=                       |  Post Request	                    |  tmd	                          |  /pet   |  data=${json_string_new2}  |  headers=${headers}  |
| |  Should Be Equal As Strings	    |  ${resp.status_code}              |  200                            |         |                            |                      |
| |  Log to console 	            |  ${resp.content}                  |                                 |         |                            |                      |

Update pet with new details
   Create Session	             tmd                      ${url}
   ${headers}=                   Create Dictionary        Content-Type=application/json
    ${json_string213}=           Get File                 /Users/shiprashekhar/synaptic/Resources/custom_keywords/pet.json
    Set Global Variable           ${json_string_new213}   ${json_string213}
   ${resp}=                      Put Request	          tmd	     /pet   data=${json_string_new213}  headers=${headers}
   Should Be Equal As Strings	 ${resp.status_code}      200
   Log to console 	             ${resp.content}

Check updated pet status
| |  Create Session	               | 	tmd                                   |  ${url} 	                     |                 |                   |
| |  ${header}=                    |    Create Dictionary                     |  Content-Type=application/json   |                 |                   |
| |  ${resp}= 	                   |    Get Request                           |  tmd                             |  /pet/10        | headers=${header} |
| |  Should Be Equal As Strings	   |  ${resp.status_code}                     |  200                             |                 |                   |
| |  Log to console 	           |  ${resp.content}                         |                                  |                 |                   |
| |  &{data} =                     |    Evaluate                              |  json.loads($resp.text)          |  json           |                   |
| |  Set Global Variable           |    ${new_status}                         |  &{data}[status]                 |                 |                   |
| |  Should Be Equal As Strings    |    ${new_status}                         |  sold                            |                 |                   |

Find pet by status
| |  Create Session	               | 	tmd                                   |  ${url} 	                     |                                     |                   |
| |  ${header}=                    |    Create Dictionary                     |  Content-Type=application/json   |                                     |                   |
| |  ${resp}= 	                   |    Get Request                           |  tmd                             |  /pet/findByStatus?status=available | headers=${header} |
| |  Should Be Equal As Strings	   |  ${resp.status_code}                     |  200                             |                                     |                   |
| |  Log to console 	           |  ${resp.content}                         |                                  |                                     |                   |
