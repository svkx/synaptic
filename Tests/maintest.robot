*** Settings ***
Documentation    Suite description
Resource    /Users/shiprashekhar/synaptic/Resources/page_objects/common_settings.robot

*** Test Cases ***
Verify user details can be created and modified
| |  [Tags]                            |       Part I                         |
| |  Create multiple users with array  |                                      |
| |  Log to console 	               |  Multiple users with array created.  |
| |  Update user with new details      |                                      |
| |  Log to console 	               |  Users detail updated.               |
| |  Get updated user details          |                                      |
| |  Log to console 	               |  Users detail are updated correctly. |



Verify pet details can be created and modified
| |  [Tags]                            |       Part II                        |
| |  Create pet details                |                                      |
| |  Log to console 	               |  Pet created.                        |
| |  Update pet with new details       |                                      |
| |  Log to console 	               |  Pet details updated.                |
| |  Check updated pet status          |                                      |
| |  Log to console 	               |  Pet status updated correctly.       |
| |  Find pet by status                |                                      |






